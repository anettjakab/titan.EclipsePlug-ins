lexer grammar Ttcn3DocCommentLexer;

/*
 ******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************
*/

/*
 * Lexer grammar for documentation comments (ETSI ES 201 873-10 V4.5.1)
 *
 * author Miklos Magyari
 */

BLOCK_BEGIN:        '/**' STAR*;
BLOCK_END:          '*/' ;

WS:	                [ \t]+ ;
AUTHOR:             '@author' WS;
DESC:               '@desc' WS;
PARAM:              '@param' WS;
SINCE:              '@since'WS;
CONFIG:             '@config' WS;
EXCEPTION:          '@exception' WS;
MEMBER:             '@member' WS;
PURPOSE:            '@purpose' WS;
REMARK:             '@remark' WS;
RETURN:             '@return' WS;
SEE:                '@see' WS;
STATUS:             '@status' WS;
URL:                '@url' WS;
VERDICT:            '@verdict' WS;
VERSION:            '@version' WS;
PRIORITY:           '@priority' WS;
REQUIREMENT:        '@requirement' WS;
REFERENCE:          '@reference' WS;
STAR:               '*' ;
NEWLINE:            (WS* STAR* WS*)? '\r'? '\n' (WS? (STAR { _input.LA(1) != '/'}?)+)?;

IDENTIFIER:         [A-Za-z][A-Za-z0-9_]* ;

FREETEXTCHAR:       [\u0021-\u0029\u002b-\u003f\u0041-\u007e\u00a1-\u00ac\u00ae-\u00ff]
                    | '*'{ _input.LA(1) != '/'}? ;

FREETEXT:           ('@@' | FREETEXTCHAR)+ ;

LINE_BEGIN:         '//*' ;
LINE:               WS* LINE_BEGIN STAR* WS* ;

