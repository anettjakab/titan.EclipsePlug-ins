package org.eclipse.titan.designer.AST;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.parsers.ttcn3parser.Ttcn3DocCommentLexer;
import org.eclipse.titan.designer.parsers.ttcn3parser.Ttcn3DocCommentParser;

/**
 * A helper class for documentation comment handling
 * 
 * ETSI ES 201 873-10 V4.5.1 (2013-04)
 * 
 * @author Miklos Magyari
 *
 */
public class DocumentComment implements ILocateableNode {
	private static final String IDPATTERN = "^[A-Za-z][A-Za-z0-9_]*$";
	
	/* Raw text of the comment */
	private String comment;
	
	/* First and last tokens of the comment */
	private Token first;
	private Token last;
	
	/* Location of the document comment. 
	 * 
	 * For now, all semantic problems will be reported for the whole comment. 
	 **/
	private Location location;
	
	private List<String> authors;
	private List<String> configs = new ArrayList<String>();
	private List<String> descs = new ArrayList<String>();
	private Map<String,String> exceptions = new HashMap<String, String>();
	private Map<String,String> members = new HashMap<String, String>();
	private Map<String,String> params = new HashMap<String, String>();
	private String purpose = null;
	private String return_ = null;
	private String since = null;
	private List<String> remarks = new ArrayList<String>();
	
	public DocumentComment(String comment, Token first, Token last) {
		this.comment = comment;
		this.first = first;
		this.last = last;
		
		authors = new ArrayList<String>();
	}
	
	public Token getFirstToken() {
		return first;
	}
	
	public Token getLastToken() {
		return last;
	}

	@Override
	public void setLocation(Location location) {
		this.location = location; 
	}

	@Override
	public Location getLocation() {
		return location;
	}
	
	public void addAuthor(String text) {
		if (text != null) {
			authors.add(text.trim());
		}
	}
	
	public void addAuthorsContent(Ttcn3HoverContent content) {
		if (getAuthors().size() > 0) {
			content.addText("\n\n");
			content.addStyledText("Authors:\n", Ttcn3HoverContent.boldRange());
			for (String author : getAuthors()) {
				content.addText("\n  ");
				content.addStyledText(author, Ttcn3HoverContent.italicRange());
			}
		}
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	
	public List<String> getDescs() {
		return descs;
	}
	
	public void addDescsContent(Ttcn3HoverContent content) {
		if (descs.size() > 0) {
			content.addText("\n\n");
			content.addStyledText("Description\n", Ttcn3HoverContent.boldRange());
			for (String desc : descs) {
				content.addText("\n" + desc);
			}
		}
	}
	
	public Map<String, String> getParams() {
		return params;
	}
	
	public void addParamsContent(Ttcn3HoverContent content) {
		content.addText("\n\n");
		content.addStyledText("Parameters\n", Ttcn3HoverContent.boldRange());
	}
	
	public Map<String, String> getMembers() {
		return members;
	}
	
	public String getReturn() {
		return return_;
	}
	
	public void addReturnContent(Ttcn3HoverContent content) {
		content.addText("\n\n");
		content.addStyledText("Returns\n", Ttcn3HoverContent.boldRange());
	}
	
	public void addConfig(String text) {
		if (text != null) {
			configs.add(text.trim());
		}
	}
	
	public void addParam(String id, String text) {
		if (id != null) {
			params.put(id.trim(), text.trim());
		}
	}
	
	public void addDesc(String text) {
		if (text != null) {
			descs.add(text.trim());
		}
	}

	public void addSince(String text) {
		if (since == null) {
			since = text.trim();
		}
	}
	
	public void addPurpose(String text) {
		if (purpose == null) {
			purpose = text.trim();
		}
	}
	
	public void addException(String id, String text) {
		if (id != null) {
			exceptions.put(id.trim(), text != null ? text.trim() : null);
		}
	}
	
	public void addMember(String id, String text) {
		if (id != null) {
			members.put(id.trim(), text != null ? text.trim() : null);
		}
	}
	
	public void addRemark(String text) {
		if (text != null) {
			remarks.add(text.trim());
		}
	}
	
	public void addReturn(String text) {
		if (return_ == null) {
			return_ = text.trim();
		}
	}
	
	public void parseComment() {		
		authors.clear();
		descs.clear();
		members.clear();
		params.clear();
		return_ = null;
		
		final Reader reader = new StringReader(comment);
		final CharStream charStream = new UnbufferedCharStream(reader);
		final Ttcn3DocCommentLexer lexer = new Ttcn3DocCommentLexer(charStream);
		lexer.setTokenFactory(new CommonTokenFactory(true));
		//List<? extends Token> tokens = lexer.getAllTokens();
		
		final CommonTokenStream cts = new CommonTokenStream(lexer);
 		final Ttcn3DocCommentParser parser = new Ttcn3DocCommentParser(cts);
		parser.pr_DocComment(this);
	}
}
