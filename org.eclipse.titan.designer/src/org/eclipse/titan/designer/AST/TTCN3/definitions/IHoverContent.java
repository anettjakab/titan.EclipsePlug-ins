/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.ui.IEditorPart;

/**
 * This interface represents the content for editor hovers
 * (information, source code, etc.)
 * 
 * @author Miklos Magyari
 */
public interface IHoverContent {
	/** 
	 * Gets information for the given node that could be displayed in a source editor hover
	 * 
	 * @param editor 
	 * @param assignment 
	 * @return
	 */
	Ttcn3HoverContent getHoverContent(IEditorPart editor, Assignment assignment);
}
