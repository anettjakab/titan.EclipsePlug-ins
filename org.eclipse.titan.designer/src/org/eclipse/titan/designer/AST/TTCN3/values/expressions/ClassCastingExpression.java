package org.eclipse.titan.designer.AST.TTCN3.values.expressions;

import java.text.MessageFormat;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.designer.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/** Represents TTCN3 class casting expressions (OOP extension)
 * 
 * @author Miklos Magyari
 *
 */
public class ClassCastingExpression extends Expression_Value {
	private static final String CASTINGTOUNRELATED = "A class can only be cast to a superclass or subclass";
	private static final String UNNECESSARYCAST = "Unnecessary cast: class is cast to itself";
	private static final String NOTACLASS = "`{0}'' side of class casting expression is not a class";
	
	private final Reference left;
	private final Type right;
	
	public ClassCastingExpression(final Reference left, final Type right) {
		this.right = right;
		this.left = left;
		
		if (left != null) {
			left.setFullNameParent(this);
		}
		if (right != null) {
			right.setFullNameParent(this);
		}
	}
	
	@Override
	public Operation_type getOperationType() {
		return Operation_type.CLASS_CASTING_OPERATION;
	}

	@Override
	public Type_type getExpressionReturntype(CompilationTimeStamp timestamp, Expected_Value_type expectedValue) {
		return Type_type.TYPE_CLASS;
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (left != null) {
			left.setMyScope(scope);
		}
		if (right != null) {
			right.setMyScope(scope);
		}
	}
	
	@Override
	public IValue evaluateValue(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}
		
		Class_Type leftClass = null;
		Class_Type rightClass = null;
		final Assignment assignment = left.getRefdAssignment(timestamp, isErroneous);
		if (assignment == null) {
			return null;
		}
		final IType refdType = assignment.getType(timestamp);
		if (refdType instanceof Referenced_Type) {
			final IType refd = ((Referenced_Type)refdType).getTypeRefdLast(timestamp);
			if (refd instanceof Class_Type) {
				IType ltype = ((Class_Type)refd).getFieldType(timestamp, left, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
				if (ltype instanceof Referenced_Type) {
					final IType rltype = ((Referenced_Type)ltype).getTypeRefd(timestamp, referenceChain);
					if (rltype instanceof Class_Type) {
						leftClass = (Class_Type)rltype;
					}
				} else if (ltype instanceof Class_Type) {
					leftClass = (Class_Type)ltype;
				} else {
					left.getLocation().reportSemanticError(MessageFormat.format(NOTACLASS, "left"));
				}
			}
		} else if (refdType instanceof Class_Type) {
			leftClass = (Class_Type)refdType;
		}
		if (right instanceof Referenced_Type) {
			final IType refd = ((Referenced_Type)right).getTypeRefdLast(timestamp);
			if (refd instanceof Class_Type) {
				rightClass = (Class_Type)refd;
			} else {
				right.getLocation().reportSemanticError(MessageFormat.format(NOTACLASS, "right"));
			}
		} else if (right instanceof Class_Type) {
			rightClass = (Class_Type)right;
		}
		if (leftClass != null && rightClass != null) {
			switch (leftClass.isRelatedClass(timestamp, rightClass)) {
			case Unrelated:
				getLocation().reportSemanticError(CASTINGTOUNRELATED);
				break;
			case Identical:
				getLocation().reportSemanticWarning(UNNECESSARYCAST);
				break;
			default:
			}
		}
		
		return null;
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isUnfoldable(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String createStringRepresentation() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected boolean memberAccept(ASTVisitor v) {
		// TODO Auto-generated method stub
		return false;
	}

}
