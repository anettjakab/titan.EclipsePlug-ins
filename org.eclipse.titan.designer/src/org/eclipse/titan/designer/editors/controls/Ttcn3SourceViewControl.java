/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlExtension2;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

/**
 * A custom TTCN3 source viewer control for the TTCN3 editor
 * Used to display code fragments in a popup window (code peek) 
 * 
 * @author Miklos Magyari
 *
 */
public class Ttcn3SourceViewControl implements IInformationControl, IInformationControlExtension2 {
	private static Ttcn3SourceViewControl instance = null;
	
	private static Shell fShell;
	private static StyledText viewer;
	
	private int maxWidth = SWT.DEFAULT;
	private int maxHeight = SWT.DEFAULT;
	
	public static Ttcn3SourceViewControl getInstance() {
		if (instance == null) {
			instance = new Ttcn3SourceViewControl();
		}
		
		return instance;
	}
	
	private Ttcn3SourceViewControl() {
		
	}
	
	public void createControl(Shell parent) {
		if (fShell != null) {
			return;
		}
		fShell = new Shell(parent, SWT.TOOL | SWT.LEFT_TO_RIGHT | SWT.RESIZE);
		Composite composite = fShell;
		GridLayout layout = new GridLayout(1, false);
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		composite.setLayout(layout);
		
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		composite.setLayoutData(gridData);
		viewer = new StyledText(composite, SWT.V_SCROLL | SWT.H_SCROLL);
		viewer.setEditable(false);
		Font textFont= JFaceResources.getFont(PreferenceConstants.EDITOR_TEXT_FONT);	
		gridData = new GridData(GridData.BEGINNING | GridData.FILL_BOTH);
		viewer.setLayoutData(gridData);
		viewer.setFont(textFont);
		
		/**
		 * event listener for ESC
		 */
		fShell.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event event) {
				switch (event.detail) {
				case SWT.TRAVERSE_ESCAPE:
					dispose();
					event.detail = SWT.TRAVERSE_NONE;
					event.doit = false;
					break;
		        }
			}
		});
		
		/**
		 * event listener for editor change 
		 */
		ISelectionService selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
		selection.addPostSelectionListener(new ISelectionListener() {
			@Override
			public void selectionChanged(IWorkbenchPart part, ISelection selection) {
				dispose();				
			}
		});
	}

	@Override
	public void setInformation(String information) {
		Ttcn3HoverContent content = new Ttcn3HoverContent(); 
		PeekSource.addStyledSource(information, content);
		viewer.setText(content.getText(HoverContentType.SOURCE));
		for (StyleRange range : content.getRanges(HoverContentType.SOURCE)) {
			viewer.setStyleRange(range);
		}
		
		final Point size = computeSizeHint(); 
		setSize(size.x, size.y);
	}

	@Override
	public void setSizeConstraints(int maxWidth, int maxHeight) {
		this.maxWidth = maxWidth;
		this.maxHeight = maxHeight;
	}

	@Override
	public Point computeSizeHint() {
		fShell.layout();
		return fShell.computeSize(SWT.DEFAULT, SWT.DEFAULT);
	}

	@Override
	public void setVisible(boolean visible) {
		fShell.setVisible(visible);
	}

	@Override
	public void setSize(int width, int height) {
		fShell.layout();
		Rectangle displayRect = fShell.getDisplay().getClientArea();
		Rectangle shellRect = fShell.getBounds();
		maxWidth = displayRect.width - shellRect.x;
		maxHeight = displayRect.height - shellRect.y; 
		fShell.setSize(Math.min(width, maxWidth), Math.min(height, maxHeight));
	}

	@Override
	public void setLocation(Point location) {
		fShell.setLocation(location);
	}

	@Override
	public void setBackgroundColor(Color background) {
		fShell.setBackground(background);
	}

	@Override
	public void setFocus() {
		fShell.setFocus();
	}

	@Override
	public void dispose() {
		instance = null;
		if (fShell != null && ! fShell.isDisposed()) {
			fShell.dispose();
		}
		fShell = null;
	}

	@Override
	public void addDisposeListener(DisposeListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeDisposeListener(DisposeListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setForegroundColor(Color foreground) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isFocusControl() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addFocusListener(FocusListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeFocusListener(FocusListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInput(Object input) {
		// TODO Auto-generated method stub
		
	}
}
