/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Color;

/**
 * This class represents a categorized collection of strings with style ranges
 * 
 * Used for ttcn3 editor hover support returned by getHoverInfo2 as an 'object'
 *
 * @author Miklos Magyari
 */
public class Ttcn3HoverContent {
	
	private class ContentEntry {
		String text;
		List<StyleRange> ranges;
		
		ContentEntry(String text, List<StyleRange> ranges) {
			this.text = text;
			this.ranges = ranges != null ? new ArrayList<StyleRange>(ranges) : new ArrayList<StyleRange>();
		}
	}
	
	private StringBuilder sb = new StringBuilder();
	private List<StyleRange> styles = new ArrayList<StyleRange>();
	
	private Map<HoverContentType, ContentEntry> map = new HashMap<HoverContentType, ContentEntry>();
	
	public void addContent(HoverContentType type, String text, List<StyleRange> range) {
		final ContentEntry mapentry = new ContentEntry(text, range);
		map.put(type, mapentry);
	}
	
	public void addContent(HoverContentType type) {
		final ContentEntry mapentry = new ContentEntry(sb.toString(), styles);
		map.put(type, mapentry);
		styles.clear();
		sb.setLength(0);
	}
	
	public boolean hasText(HoverContentType type) {
		final ContentEntry mapentry = map.get(type);
		if (mapentry == null) {
			return false;
		}
		if (mapentry.text == null) {
			return false;
		}
		return true;
	}
	
	public String getText(HoverContentType type) {
		final ContentEntry mapentry = map.get(type);
		if (mapentry == null) {
			return null;
		}
		return mapentry.text;
	}
	
	public List<StyleRange> getRanges(HoverContentType type) {
		final ContentEntry mapentry = map.get(type);
		if (mapentry == null) {
			return null;
		}
		return mapentry.ranges;
	}
	
	public static StyleRange boldRange() {
		final StyleRange range = new StyleRange();
		range.fontStyle = SWT.BOLD;
		
		return range;
	}
	
	public static StyleRange italicRange() {
		final StyleRange range = new StyleRange();
		range.fontStyle = SWT.ITALIC;
		
		return range;
	}
	
	public static StyleRange colorRange(Color color) {
		final StyleRange range = new StyleRange();
		range.foreground = color;
		
		return range;
	}
	
	public static StyleRange boldItalicRange() {
		final StyleRange range = new StyleRange();
		range.fontStyle = SWT.BOLD | SWT.ITALIC;
		
		return range;
	}
	
	public void addText(String text) {
		sb.append(text);
	}
	
	public void addStyledText(String text, StyleRange style) {
		if (style != null) {
			style.start = sb.length();
			style.length = text.length();
			styles.add(style);
		}
		sb.append(text);
	}
}
