/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.text.AbstractInformationControl;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IInformationControlExtension2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.editors.text.EditorsUI;

/** 
 * Control for handling ttcn3 source editor hovers
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 *
 */
public class Ttcn3HoverInfoControl extends AbstractInformationControl implements IInformationControlExtension2 {
	private enum ViewMode { CodeHover, MarkerHover }
	
	private StyledText viewer;
	private Ttcn3HoverContent content;
	private MarkerHoverContent markerContent;
	private Shell fShell;
	private boolean isRich;
	private HoverContentType actualType = HoverContentType.loadAsProperty();
	private ToolBarManager toolbar;
	private ViewMode viewMode;
	private Composite fParent;

	private int maxWidth = SWT.DEFAULT;
	private int maxHeight = SWT.DEFAULT;
	private int toolbarTextSize = 0;

	private IAction switchAction = new Action("", IAction.AS_PUSH_BUTTON) {
		@Override
        public void run() {
			switch (actualType) {
			case INFO:
				actualType = HoverContentType.SOURCE;
				break;
			case SOURCE:
				actualType = HoverContentType.INFO;
				break;
			}
			setText(getButtonText());
			HoverContentType.storeAsProperty(actualType);
			setHoverContentToActual();
			setSize(computeSizeHint());
			toolbar.update(true);
        }
	};
	
	public Ttcn3HoverInfoControl(Shell parentShell, String string) {
		super(parentShell, string);
		create();
	}

	public Ttcn3HoverInfoControl(Shell parentShell, String string, final boolean isRich, final ToolBarManager toolbar) {
		super(parentShell, toolbar);
		this.toolbar = toolbar;
		this.isRich = isRich;
		create();
	}

	@Override
	@Deprecated
	public void setInformation(String information) {
		// setInput is used instead
	}

	@Override
	public void setFocus() {
		super.setFocus();
		if (fShell != null) {
			fShell.setFocus();
		}
	}

	@Override
	public boolean hasContents() {
		return content != null || markerContent != null;
	}

	@Override
	protected void createContent(Composite parent) {
		fParent = parent;
		fShell = parent.getShell();
		viewer = new StyledText(parent, isRich ? (SWT.H_SCROLL | SWT.V_SCROLL) : 0);
		viewer.setEditable(false);
		viewer.setBackground(fShell.getBackground());
		
		setStatusText("Hover to focus");
	}

	@Override
	public IInformationControlCreator getInformationPresenterControlCreator() {
		return new IInformationControlCreator() {
			@Override
            public IInformationControl createInformationControl(Shell parent) {
				ToolBarManager toolbar = new ToolBarManager(SWT.FLAT | SWT.VERTICAL);
            	return new Ttcn3HoverInfoControl(parent, EditorsUI.getTooltipAffordanceString(), true, toolbar);
            }
		};
	}

	@Override
	public void setInput(final Object input) {
		if (input instanceof Ttcn3HoverContent) {
			viewMode = ViewMode.CodeHover;
			content = (Ttcn3HoverContent)input;
			if (content.hasText(actualType)) {
				setHoverContentToActual();
			}
		}
		if (input instanceof MarkerHoverContent) {
			viewMode = ViewMode.MarkerHover;
			markerContent = (MarkerHoverContent)input;
			setHoverContentToActual();
		}
	}

	private void setHoverContentToActual() {
		switch (viewMode) {
		case CodeHover:
			final String text = content.getText(actualType);
			if (text != null) {
				viewer.setText(text);
				for (StyleRange style : content.getRanges(actualType)) {
					viewer.setStyleRange(style);
				}
			};
			if (isRich && toolbar != null) {
				final ToolBar tb = toolbar.createControl(fParent);
				tb.setCursor(Display.getCurrent().getSystemCursor(SWT.CURSOR_HAND));
				tb.setForeground(fShell.getDisplay().getSystemColor(SWT.COLOR_LINK_FOREGROUND));

				switchAction.setText(getButtonText());
				if (toolbar.getItems().length == 0) {
					toolbar.add(switchAction);
				}
				toolbar.update(true);
				toolbarTextSize = tb.getSize().x;
			}
			break;
		case MarkerHover:
			final String markersText = markerContent.getText();
			if (markersText != null) {
				viewer.setText(markersText);
			}
			if (isRich && toolbar != null) {
				final int nrOfProposals = markerContent.nrOfProposals();
				final ToolBar tb = toolbar.createControl(fParent);
				for (int i = 0; i < nrOfProposals; i++) {
					final HoverProposal prop = markerContent.getProposal(i);
					IAction propAction = new Action("", IAction.AS_PUSH_BUTTON) {
						@Override
						public void run() {
							prop.run(null);
							dispose();
						}
					};
					propAction.setText(prop.getLabel());
					toolbar.add(propAction);
					toolbar.update(true);
					toolbarTextSize = tb.getSize().x;
					setSize(computeSizeHint());
				}
			}
		}
	}

	@Override
	public void setSizeConstraints(final int maxWidth, final int maxHeight) {
		this.maxWidth = maxWidth;
		this.maxHeight = maxHeight;
	}

	@Override
	public Point computeSizeHint() {
		fShell.layout();
		return fShell.computeSize(SWT.DEFAULT, SWT.DEFAULT);
	}

	@Override
	public void setSize(final int width, final int height) {
		fShell.layout();
		final Rectangle displayRect = fShell.getDisplay().getClientArea();
		final Rectangle shellRect = fShell.getBounds();
		maxWidth = displayRect.width - shellRect.x;
		maxHeight = displayRect.height - shellRect.y;
		fShell.setSize(Math.min(Math.max(width + 15, toolbarTextSize + 15), maxWidth), Math.min(height + 15, maxHeight));
	}

	public void setSize(Point size) {
		setSize(size.x, size.y);
	}
	
	private String getButtonText() {
		return "Click to switch to " + (actualType == HoverContentType.INFO ? HoverContentType.SOURCE : HoverContentType.INFO) + " view"; 	
	}
}
